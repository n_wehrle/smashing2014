# SmashingConf 2014 (Oxford) Retrospective
I attended the SmashingConf 2014 in Oxford. I tried to summarize the whole conference in an 1 hour talk. These are the slides.

![Coverpic](https://bitbucket.org/n_wehrle/smashing2014/raw/master/coverpic.jpg)

## Topics
- Web Components
- Polyfills
- CSS Color
- New Image Formats
- Flexbox
- CSS tidbits
- CSS Gradients

## CSS-based SlideShow System

Warning: Only works in latest Firefox, Opera, Safari or Chrome.
For more information, see the [sample slideshow](http://lea.verou.me/csss/sample-slideshow.html)